import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:ui/routes/auth.dart';
import 'firebase_options.dart';
import 'package:flutterfire_ui/auth.dart';
import 'package:flutterfire_ui/i10n.dart';
import 'conf/conf.dart';
import 'package:firebase_auth/firebase_auth.dart';
import './routes/url_strategy.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

const bool USE_EMULATOR = true;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  if (USE_EMULATOR) {
    await _connectToFirebaseEmulator();
  }

  await FirebaseAuth.instance.setPersistence(Persistence.LOCAL);

  FlutterFireUIAuth.configureProviders([
    const EmailProviderConfiguration(),
    const GoogleProviderConfiguration(clientId: GOOGLE_CLIENT_ID),
  ]);

  usePathUrlStrategy();

  runApp(App());
}

Future _connectToFirebaseEmulator() async {
  FirebaseFirestore.instance.useFirestoreEmulator('localhost', 8080);

  await FirebaseAuth.instance.useAuthEmulator('localhost', 9099);
}

// Overrides a label for en locale
// To add localization for a custom language follow the guide here:
// https://flutter.dev/docs/development/accessibility-and-localization/internationalization#an-alternative-class-for-the-apps-localized-resources
class LabelOverrides extends DefaultLocalizations {
  const LabelOverrides();

  @override
  String get emailInputLabel => 'Enter your email';
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        useMaterial3: true,
        primarySwatch: Colors.amber,
      ),
      initialRoute: AuthRouteManager.initialRoute,
      onGenerateRoute: AuthRouteManager.generateRoute,
      title: 'RunClubLive',
      debugShowCheckedModeBanner: false,
      locale: const Locale('en'),
      localizationsDelegates: [
        FlutterFireUILocalizations.withDefaultOverrides(const LabelOverrides()),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        FlutterFireUILocalizations.delegate,
      ],
    );
  }
}
