import 'package:flutterfire_ui/auth.dart';
import 'package:flutter/material.dart';
import '../components/decorations.dart';
import '../routes/auth.dart';

Widget getVerifyEmailPage(BuildContext context) {
  return EmailVerificationScreen(
    headerBuilder: headerIcon(Icons.verified),
    sideBuilder: sideIcon(Icons.verified),
    // actionCodeSettings: actionCodeSettings,
    actions: [
      EmailVerified(() {
        Navigator.pushReplacementNamed(context, AuthRouteManager.appFlow);
      }),
      Cancel((context) {
        FlutterFireUIAuth.signOut(context: context);
        Navigator.pushReplacementNamed(context, AuthRouteManager.signInPage);
      }),
    ],
  );
}
