import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:ui/models/run.dart';
import 'package:ui/components/trainer-avatar.dart';
import 'package:relative_time/relative_time.dart';
import 'package:ui/models/user.dart';
import 'package:intl/intl.dart';
import 'package:firebase_auth/firebase_auth.dart' show FirebaseAuth;
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';

class RunDetailView extends StatefulWidget {
  final String runId;

  RunDetailView(this.runId);

  @override
  _RunDetailViewState createState() => _RunDetailViewState();
}

class _RunDetailViewState extends State<RunDetailView> {
  Run? _run;
  User? _coach;
  bool _currentUserHasJoined = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    this.fetchDataFromFirestore();
  }

  String _runTimeString() {
    final startTime = this._run?.startsAt ?? DateTime.now();

    final relativeDate = RelativeTime.locale(
      const Locale('en'),
      timeUnits: [TimeUnit.day],
    ).format(startTime);

    return '$relativeDate - ${DateFormat('hh:mm a').format(startTime)}';
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(slivers: [
      // Add the app bar to the CustomScrollView.
      SliverAppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back),
        ),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).pushNamed('/${widget.runId}/chat');
            },
            icon: const Icon(Icons.chat),
          )
        ],
        // Provide a standard title.
        title: Text(this._run?.title ?? ''),
        centerTitle: true,
        // Allows the user to reveal the app bar if they begin scrolling
        // back up the list of items.
        floating: true,
        // Display a placeholder widget to visualize the shrinking size.
        flexibleSpace: Placeholder(),
        // Make the initial height of the SliverAppBar larger than normal.
        expandedHeight: 100,
      ),
      SliverToBoxAdapter(
          child: Container(
              child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          !this._currentUserHasJoined
              ? TrainerAvatar(photoUrl: this._coach?.photoUrl)
              : CountdownTimer(
                  endTime: this._run?.startsAt?.millisecondsSinceEpoch,
                ),
          Text('${this._coach?.firstName} ${this._coach?.lastName}',
              style: TextStyle(fontSize: 14, color: Colors.grey)),
          Text('${this._run?.durationInMinutes} min - ${this._run?.title}',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
          Text(_runTimeString()),
          Row(
            children: [
              Text.rich(
                TextSpan(
                  children: [
                    TextSpan(text: this._run?.durationInMinutes.toString()),
                    WidgetSpan(child: Icon(Icons.punch_clock))
                  ],
                ),
              ),
              Text.rich(
                TextSpan(
                  children: [
                    TextSpan(text: this._run?.userCount.toString()),
                    WidgetSpan(child: Icon(Icons.people))
                  ],
                ),
              ),
            ],
          ),
          Text(this._run?.description ?? '',
              style: TextStyle(fontSize: 14, color: Colors.grey)),
          !this._currentUserHasJoined
              ? ElevatedButton(
                  onPressed: () {
                    setUserRaceStatus(true);
                  },
                  child: Text('Sign Up'),
                )
              : Row(children: [
                  ElevatedButton(
                    onPressed: () {
                      // TODO: figure out what challenge friends does.
                    },
                    child: Text('Challenge Friends'),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      setUserRaceStatus(false);
                    },
                    child: Text('Cancel Run'),
                  )
                ]),
          // create widgets for each tab bar here
        ],
      ))),
    ]);
  }

  Future setUserRaceStatus(bool hasJoined) async {
    if (widget.runId == '') {
      throw Exception('could not toggle user race status: invalid run id');
    }

    if (FirebaseAuth.instance.currentUser == null) {
      throw Exception(
          'could not toggle user race status: the current user is null');
    }

    if (this._currentUserHasJoined == hasJoined) {
      return;
    }

    final runDoc =
        FirebaseFirestore.instance.collection('runs').doc(widget.runId);

    final userDoc =
        runDoc.collection('users').doc(FirebaseAuth.instance.currentUser?.uid);

    if (hasJoined) {
      await userDoc.set({'registeredAt': Timestamp.now()});
    } else {
      await userDoc.delete();
    }

    await runDoc
        .update({'userCount': FieldValue.increment(hasJoined ? 1 : -1)});

    // _run.userCount += 1;
    // _run.currentUserHasJoinedRun = hasJoined;
    // setState(() {
    //   _run = _run;
    // });
    // TODO: revert to more efficient implementation aboe once testing and validation is complete.
    await fetchDataFromFirestore();
  }

  // TODO: Should find a way to make this streaming, so updates in user count are reflected.
  Future fetchDataFromFirestore() async {
    final run = await fetchRunFromFirestore(widget.runId);
    final coach = await fetchUserFromFirestore(run.coachId!);
    final currentUserDoc = await FirebaseFirestore.instance
        .doc(
            '/runs/${widget.runId}/users/${FirebaseAuth.instance.currentUser?.uid}')
        .get();

    setState(() {
      this._run = run;
      this._coach = coach;
      this._currentUserHasJoined = currentUserDoc.exists;
    });
  }
}
