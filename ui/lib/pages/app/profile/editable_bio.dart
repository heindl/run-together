import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutterfire_ui/auth.dart';
import 'package:flutterfire_ui/i10n.dart';
import 'package:flutter/material.dart';
import 'subtitle.dart';

class EditableUserBio extends StatefulWidget {
  final String userID;
  final String? userBio;

  const EditableUserBio({
    Key? key,
    required this.userID,
    this.userBio,
  }) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _EditableUserBioState createState() => _EditableUserBioState();
}

class _EditableUserBioState extends State<EditableUserBio> {
  String? _userBio;
  final firestoreInstance = FirebaseFirestore.instance;

  @override
  void didUpdateWidget(old) {
    super.didUpdateWidget(old);
    setState(() {
      _userBio = widget.userBio ?? '';
    });
  }

  late final ctrl = TextEditingController(text: _userBio ?? '');

  late bool _editing = false;

  bool _isLoading = false;

  void _onEdit() {
    setState(() {
      _editing = true;
    });
  }

  Future<void> _finishEditing() async {
    try {
      if (_userBio == ctrl.text) return;

      setState(() {
        _isLoading = true;
      });

      await firestoreInstance
          .collection("users")
          .doc(widget.userID)
          .update({"bio": ctrl.text});
    } finally {
      setState(() {
        _userBio = ctrl.text;
        _editing = false;
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final l = FlutterFireUILocalizations.labelsOf(context);
    late Widget iconButton;

    iconButton = IconButton(
      icon: Icon(_editing ? Icons.check : Icons.edit),
      color: theme.colorScheme.secondary,
      onPressed: _editing ? _finishEditing : _onEdit,
    );

    if (!_editing) {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 5.5),
        child: IntrinsicWidth(
          child: Row(
            children: [
              Subtitle(text: _userBio ?? 'Unknown'),
              iconButton,
            ],
          ),
        ),
      );
    }

    late Widget textField;

    textField = TextField(
      autofocus: true,
      controller: ctrl,
      decoration: InputDecoration(hintText: l.profile, labelText: l.profile),
      onSubmitted: (_) => _finishEditing(),
    );

    return Row(
      children: [
        Expanded(child: textField),
        const SizedBox(width: 8),
        SizedBox(
          width: 50,
          height: 32,
          child: Stack(
            children: [
              if (_isLoading)
                const LoadingIndicator(size: 24, borderWidth: 1)
              else
                Align(
                  alignment: Alignment.topLeft,
                  child: iconButton,
                ),
            ],
          ),
        ),
      ],
    );
  }
}
