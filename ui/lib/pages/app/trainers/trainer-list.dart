import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:ui/models/user.dart';
import 'package:flutterfire_ui/firestore.dart';
import 'package:ui/components/trainer-avatar.dart';

class TrainerListCard extends StatelessWidget {
  final User _trainer;

  TrainerListCard(this._trainer);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: InkWell(
            onTap: () {
              Navigator.of(context).pushNamed('/' + this._trainer.uid!);
            },
            child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TrainerAvatar(photoUrl: _trainer.photoUrl),
                    Text('${_trainer.firstName} ${_trainer.lastName}',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18)),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: new List<Widget>.generate(5,
                            (i) => Icon(Icons.star, color: Colors.amberAccent)))
                  ],
                ))));
  }
}

class TrainerListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: FirestoreQueryBuilder(
            query: FirebaseFirestore.instance
                .collection('users')
                // TODO: Choose either trainer or coach and stick with it.
                .where('role', isEqualTo: 'coach')
                .withConverter<User>(
                    fromFirestore: (snapshot, _) => User.fromSnapshot(snapshot),
                    toFirestore: (obj, _) => obj.toJson()),
            builder: (context, snapshot, _) {
              if (snapshot.hasError) {
                throw Exception(
                    'could not fetch coach list: ${snapshot.error}');
              }

              if (snapshot.isFetching) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }

              return GridView.builder(
                itemCount: snapshot.docs.length,
                shrinkWrap: true,
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2),
                itemBuilder: (BuildContext context, int index) {
                  return TrainerListCard(snapshot.docs[index].data() as User);
                },
              );
            }));
  }
}
