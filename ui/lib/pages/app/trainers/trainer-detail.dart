import 'package:flutter/material.dart';
import 'package:ui/models/user.dart';
import 'trainer-detail-run-list.dart';
import 'package:ui/components/trainer-avatar.dart';

class TrainerProfileView extends StatefulWidget {
  final String userId;

  TrainerProfileView(this.userId);

  @override
  _TrainerProfileViewState createState() => _TrainerProfileViewState();
}

class _TrainerProfileViewState extends State<TrainerProfileView> {
  User _trainer = User();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    fetchUserFromFirestore(widget.userId)
        .then((coach) => setState(() {
              this._trainer = coach;
            }))
        .catchError((err) => throw Exception(err));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: DefaultTabController(
            length: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                TrainerAvatar(photoUrl: _trainer.photoUrl),
                Text('${this._trainer.firstName} ${this._trainer.lastName}',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                Text('${this._trainer.city}, ${this._trainer.state}',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
                Container(
                  height: 50,
                  child: AppBar(
                    bottom: TabBar(
                      tabs: [
                        Tab(
                          text: 'About',
                        ),
                        Tab(text: 'Schedule'),
                      ],
                    ),
                  ),
                ),

                // create widgets for each tab bar here
                Expanded(
                  child: TabBarView(
                    children: [
                      // first tab bar view widget
                      Container(
                        child: Center(
                          child: Text(
                            this._trainer.description!,
                          ),
                        ),
                      ),

                      // second tab bar viiew widget
                      TrainerDetailRunList(widget.userId),
                    ],
                  ),
                ),
              ],
            )));
  }
}
