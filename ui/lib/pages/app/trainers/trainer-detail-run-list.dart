import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:ui/models/run.dart';
import 'package:relative_time/relative_time.dart';
import 'package:flutterfire_ui/firestore.dart';

class TrainerDetailRunList extends StatelessWidget {
  final String trainerId;
  TrainerDetailRunList(this.trainerId);

  @override
  Widget build(BuildContext context) {
    return FirestoreListView(
        query: FirebaseFirestore.instance
            .collection('runs')
            .where('coachId', isEqualTo: this.trainerId)
            .where('startsAt', isGreaterThanOrEqualTo: new Timestamp.now())
            .orderBy('startsAt')
            .withConverter<Run>(
                fromFirestore: (snapshot, _) => Run.fromSnapshot(snapshot),
                toFirestore: (run, _) => run.toJson()),
        itemBuilder: (context, snapshot) {
          final run = snapshot.data()! as Run;

          return Container(
              child: Column(
            children: [
              Text(RelativeTime.locale(const Locale('en'))
                  .format(run.startsAt!)),
              TrainerDetailRunListCard(
                  run.userCount!, run.durationInMinutes!, run.title!),
            ],
          ));
        });
  }
}

class TrainerDetailRunListCard extends StatelessWidget {
  final int durationInMinutes;
  final String title;
  final int userCount;

  TrainerDetailRunListCard(this.userCount, this.durationInMinutes, this.title);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: InkWell(
            onTap: () {
              print('should navigate to run detail page');
              // Navigator.of(context).pushNamed('/' + _run.uid);
            },
            child: Padding(
              padding: const EdgeInsets.all(3.0),
              child: Card(
                margin: EdgeInsets.all(15),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                elevation: 5,
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Row(
                    children: [
                      Expanded(flex: 2, child: Icon(Icons.calendar_today)),
                      Expanded(
                        flex: 8,
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(title),
                              Text('$durationInMinutes min',
                                  style: TextStyle(color: Colors.grey)),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                          flex: 4,
                          child: Text.rich(
                            TextSpan(
                              children: [
                                TextSpan(text: userCount.toString()),
                                WidgetSpan(child: Icon(Icons.people))
                              ],
                            ),
                          )),
                    ],
                  ),
                ),
              ),
            )));
  }
}
