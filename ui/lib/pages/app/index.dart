import 'package:flutter/material.dart';
import 'package:ui/routes/app.dart';

class AppIndex extends StatefulWidget {
  const AppIndex({
    super.key,
    required this.route,
  });

  final String route;

  @override
  AppIndexState createState() => AppIndexState();
}

class AppIndexState extends State<AppIndex> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  int _currentPageIndex = 0;

  // int _currentPageIndex() {
  //   print("current page index" + widget.route);
  //   switch(widget.route) {
  //     case AppRouteManager.browsePage:
  //       return 0;
  //     case AppRouteManager.onDemandPage:
  //       return 1;
  //     case AppRouteManager.liveRunPage:
  //       return 2;
  //     case AppRouteManager.trainersPage:
  //       return 3;
  //     case AppRouteManager.profilePage:
  //       return 4;
  //     default:
  //       return 0;
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentPageIndex,
        onTap: (int index) {
          setState(() {
            _currentPageIndex = index;
          });
          switch (index) {
            case 0:
              _navigatorKey.currentState!.pushNamed(AppRouteManager.browsePage);
              break;
            case 1:
              _navigatorKey.currentState!
                  .pushReplacementNamed(AppRouteManager.runsPage);
              break;
            case 2:
              _navigatorKey.currentState!
                  .pushReplacementNamed(AppRouteManager.trainersPage);
              break;
            case 3:
              _navigatorKey.currentState!
                  .pushReplacementNamed(AppRouteManager.profilePage);
              break;
          }
        },
        // selectedIndex: currentPageIndex,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            activeIcon: Icon(Icons.content_paste_search_outlined),
            icon: Icon(Icons.content_paste_search),
            label: 'Browse',
          ),
          BottomNavigationBarItem(
            activeIcon: Icon(Icons.access_time_filled),
            icon: Icon(Icons.access_time_outlined),
            label: 'Live',
          ),
          BottomNavigationBarItem(
            activeIcon: Icon(Icons.accessibility_new_outlined),
            icon: Icon(Icons.accessibility_new),
            label: 'Trainers',
          ),
          BottomNavigationBarItem(
            activeIcon: Icon(Icons.account_circle_sharp),
            icon: Icon(Icons.account_circle_outlined),
            label: 'Profile',
          ),
        ],
      ),
      body: Navigator(
        key: _navigatorKey,
        initialRoute: widget.route,
        onGenerateRoute: AppRouteManager.generateRoute,
      ),
    );
  }
}
