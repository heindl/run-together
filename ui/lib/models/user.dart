import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  String? uid;
  String? description;
  String? firstName;
  String? lastName;
  String? role;
  String? city;
  String? state;

  User(
      {this.uid,
      this.firstName,
      this.lastName,
      this.description,
      this.role,
      this.city,
      this.state});

  String get photoUrl =>
      this.uid != null && this.uid != '' ? '/avatars/${this.uid}.jpeg' : '';

  User.fromSnapshot(DocumentSnapshot<Map<String, Object?>> snapshot)
      : this(
          uid: snapshot.id,
          description: snapshot.data()?['description']! as String,
          firstName: snapshot.data()?['firstName']! as String,
          lastName: snapshot.data()?['lastName']! as String,
          role: snapshot.data()?['role']! as String,
          city: snapshot.data()?['city']! as String,
          state: snapshot.data()?['state']! as String,
        );

  Map<String, Object?> toJson() {
    return {
      'description': description,
      'firstName': firstName,
      'lastName': lastName,
      'role': role,
      'city': city,
      'state': state,
    };
  }
}

Future<User> fetchUserFromFirestore(String id) async {
  if (id == '') {
    throw Exception('could not fetch user: invalid user id');
  }

  final collection = FirebaseFirestore.instance.collection('users').withConverter<User>(
            fromFirestore: (snapshot, _) => User.fromSnapshot(snapshot),
            toFirestore: (User model, _) => model.toJson(),
          );

  final doc = await collection.doc(id).get();

  return doc.data()!;
}
