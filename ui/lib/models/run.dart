import 'package:cloud_firestore/cloud_firestore.dart';

class Run {
  String? uid;
  DateTime? startsAt;
  String? title;
  String? description;
  int? durationInMinutes;
  int? userCount;
  String? coachId;

  Run(
      {this.uid,
      this.startsAt,
      this.title,
      this.description,
      this.durationInMinutes,
      this.userCount,
      this.coachId});

  Run.fromSnapshot(DocumentSnapshot<Map<String, Object?>> snapshot)
      : this(
          uid: snapshot.id,
          startsAt: (snapshot.data()?['startsAt']! as Timestamp).toDate(),
          title: snapshot.data()?['title']! as String,
          description: snapshot.data()?['description']! as String,
          durationInMinutes: snapshot.data()?['durationInMinutes']! as int,
          userCount: snapshot.data()?['userCount']! as int,
          coachId: snapshot.data()?['coachId']! as String,
        );

  Map<String, Object?> toJson() {
    return {
      'startsAt': Timestamp.fromDate(startsAt!),
      'title': title,
      'description': description,
      'durationInMinutes': durationInMinutes,
      'userCount': userCount,
      'coachId': coachId,
    };
  }
}

Future<Run> fetchRunFromFirestore(String id) async {
  if (id == '') {
    throw Exception('could not fetch run: invalid run id');
  }

  final collection = FirebaseFirestore.instance.collection('runs').withConverter<Run>(
            fromFirestore: (snapshot, _) => Run.fromSnapshot(snapshot),
            toFirestore: (Run model, _) => model.toJson(),
          );

  final doc = await collection.doc(id).get();

  final response = doc.data()!;

  return response;
}
