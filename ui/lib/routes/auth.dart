import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ui/pages/app/index.dart';
import '../pages/sign-in.dart';
import '../pages/verify-email.dart';
import '../pages/forgot-password.dart';
import '../pages/404.dart';

class AuthRouteManager {
  static const String appFlow = '/app';
  static const String signInPage = '/sign-in';
  static const String verifyEmailPage = '/verify-email';
  static const String forgotPasswordPage = '/forgot-password';

  static String get initialRoute {
    final auth = FirebaseAuth.instance;

    if (auth.currentUser == null) {
      return signInPage;
    }

    if (!auth.currentUser!.emailVerified && auth.currentUser!.email != null) {
      return verifyEmailPage;
    }

    return appFlow;
  }

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case signInPage:
        return MaterialPageRoute(builder: getSignInPage);
      case verifyEmailPage:
        return MaterialPageRoute(builder: getVerifyEmailPage);
      case forgotPasswordPage:
        return MaterialPageRoute(builder: getForgotPasswordPage);
      default:
        if (settings.name!.startsWith(appFlow)) {
          final subRoute = settings.name!.substring(appFlow.length);
          return MaterialPageRoute(
              builder: (context) => AppIndex(route: subRoute));
        }
        return MaterialPageRoute(builder: (context) => NotFoundPage());
    }
  }
}
