import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

class TrainerAvatar extends StatefulWidget {
  final String? photoUrl;

  const TrainerAvatar({
    super.key,
    this.photoUrl,
  });

  @override
  _TrainerAvatarState createState() => _TrainerAvatarState();
}

class _TrainerAvatarState extends State<TrainerAvatar> {
  String? _photoUrl;
  final _avatarPlaceholderColor = Colors.grey;
  final _avatarSize = 120.0;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _getImageRef(widget.photoUrl ?? '');
  }

  @override
  void didUpdateWidget(old) {
    super.didUpdateWidget(old);
    _getImageRef(widget.photoUrl ?? '');
  }

  Widget _imageFrameBuilder(
    BuildContext context,
    Widget? child,
    int? frame,
    bool? _,
  ) {
    if (frame == null) {
      return Container(color: _avatarPlaceholderColor);
    }

    return child!;
  }

  Future _getImageRef(String path) async {
    // Only set once.
    if (_photoUrl != null) {
      return;
    }
    if (path == '') {
      return;
    }
    final ref = FirebaseStorage.instance.ref().child(path);
// no need of the file extension, the name will do fine.
    var url = await ref.getDownloadURL();
    setState(() {
      _photoUrl = url;
    });
  }

  @override
  Widget build(BuildContext context) {
    print('building trainer avatar with image: ' + (widget.photoUrl ?? ''));
    return SizedBox(
      height: _avatarSize,
      width: _avatarSize,
      child: ClipPath(
        clipper: ShapeBorderClipper(shape: const CircleBorder()),
        clipBehavior: Clip.hardEdge,
        child: _photoUrl != null
            ? Image.network(
                _photoUrl!,
                width: _avatarSize,
                height: _avatarSize,
                cacheWidth: _avatarSize.toInt(),
                cacheHeight: _avatarSize.toInt(),
                fit: BoxFit.cover,
                frameBuilder: _imageFrameBuilder,
              )
            : Center(
                child: Icon(
                  Icons.account_circle,
                  size: _avatarSize,
                  color: _avatarPlaceholderColor,
                ),
              ),
      ),
    );
  }
}
